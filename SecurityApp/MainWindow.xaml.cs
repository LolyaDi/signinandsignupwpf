﻿using Nemiro.OAuth.LoginForms;
using SecurityApp.DataAccess;
using SecurityApp.Models;
using SecurityApp.Services;
using System.Linq;
using System.Windows;

namespace SecurityApp
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void SignInButtonClick(object sender, RoutedEventArgs e)
        {
            var login = loginTextBox.Text;
            var password = passwordBox.Password;

            if (string.IsNullOrEmpty(login) || string.IsNullOrEmpty(password))
            {
                MessageBox.Show("Enter data");
                return;
            }

            using (var context = new SecurityContext())
            {
                var user = context.Users.SingleOrDefault(u => u.Login == login);

                if (user == null || !SecurityHasher.VerifyPassword(password, user.Password))
                {
                    MessageBox.Show("Login or password isn't correct!");
                }
                else
                {
                    MessageBox.Show("Authentication successed!");
                }
            }
        }

        private void SignUpButtonClick(object sender, RoutedEventArgs e)
        {
            var login = newUserLoginTextBox.Text;
            var password = newUserPasswordBox.Password;

            if (string.IsNullOrEmpty(login) || string.IsNullOrEmpty(password))
            {
                MessageBox.Show("Enter data");
                return;
            }
            
            using (var context = new SecurityContext())
            {
                var users = context.Users.ToList();

                if (users.Find(user => user.Login == login) != null)
                {
                    MessageBox.Show("User already exists");
                    return;
                }

                context.Users.Add(new User
                {
                    Login = login,
                    Password = SecurityHasher.HashPassword(password)
                });

                MessageBox.Show("Congratulations! You've successfully signed up!");
            }
        }

        private void OpenSignUpDialogButtonClick(object sender, RoutedEventArgs e)
        {
            signUpDialog.IsOpen = true;
        }

        private void SocialMediaSignIn(Login login)
        {
            login.ShowDialog();

            if (login.IsSuccessfully)
            {
                Properties.Settings.Default.SettingsKey = login.AccessTokenValue;
                MessageBox.Show("Authentication successed!");
            }
            else
            {
                MessageBox.Show("Authentication failed! Try again!");
            }
        }

        private void VkontakteSignInButtonClick(object sender, RoutedEventArgs e)
        {
            SocialMediaSignIn(new VkontakteLogin
                (
                "6968848",
                "YN7OxndM70ZYmc4v446X",
                true
                ));
        }

        private void MailRuSignInButtonClick(object sender, RoutedEventArgs e)
        {
            SocialMediaSignIn(new MailRuLogin
                (
                "765388",
                "4e5a02e61037f54978119c530065da19",
                true
                ));
        }

        private void TwitterSignInButtonClick(object sender, RoutedEventArgs e)
        {
            SocialMediaSignIn(new TwitterLogin
                (
                "pDIngJdncVz63dhh4e0ch635I",
                "PjcXmLisSjGEulIUyJWGPfQjV4mJqaY1wRQylDdTBycVrnynzD",
                true
                ));
        }
    }
}
